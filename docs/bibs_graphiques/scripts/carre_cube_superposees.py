# --- PYODIDE:env --- #

import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
plt = PyodidePlot('cible_double')  


# --- PYODIDE:code --- #

xs1 = [-3 + k * 0.1 for k in range(61)]
ys1 = [x**2 for x in xs1]

xs2 = [-2 + k * 0.1 for k in range(41)]
ys2 = [x**3 for x in xs2]

plt.plot(xs1, ys1, "r-", xs2, ys2, "b+")  
plt.grid()  # Optionnel : pour voir le quadrillage
plt.axhline()  # Optionnel : pour voir l'axe des abscisses
plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt.title("La fonction carré et la fonction cube")
plt.show()
