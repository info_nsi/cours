---
author: Mireille Coilhac
title: Images  en Python
---

!!! info "Utiliser les bibliothèques graphiques"

    Utiliser les bibliothèques graphiques n'est pas tout à fait immédiat.

    Voici deux exemples.



## I. Utiliser la bibliothèque matplotlib 

!!! info "Le principe"

    Le principe est d'appeler : 

    * Une fois la macro
    ```markdown title=""
    {% raw %}
    {{ IDE(...) }}
    {% endraw %}
    ```

    * Autant de fois que de fenêtres graphiques désirées, la macro :
    ```markdown title=""
    {% raw %}
    {{ figure(...) }}
    {% endraw %}
    ``` 
    

!!! info "Une seule fenêtre"

    * Dans le cas où l'on ne désire qu'une fenêtre, il suffit d'appeler : 
    ```markdown title=""
    {% raw %}
    {{ figure() }}
    {% endraw %}
    ``` 

    * La section `# --- PYODIDE:env --- #` du fichier Python, qui n'apparait pas dans 
    le rendu du site, contiendra par exemple :   
    ```python title=""
    import matplotlib
    plt = PyodidePlot()  
    ```

!!! info "Plusieurs fenêtres"
    
    * Dans le cas où l'on désire plusieurs fenêtres, il faut les identifier. On appelera donc 
    par exemple pour deux fenêtres :  
    ```md title=""
    {% raw %}
    {{ figure('cible_1') }}

    {{ figure('cible_2') }}
    {% endraw %}
    ```

    * La section `# --- PYODIDE:env --- #` du fichier Python, qui n'apparait pas 
    dans le rendu du site, contiendra alors par exemple, pour être cohérent :  
    ```python title=""
    import matplotlib
    plt1 = PyodidePlot('cible_1')  
    plt2 = PyodidePlot('cible_2')
    ```
    
### Tracé d'un seul graphique dans une fenêtre

!!! info ""
    
    ```markdown title="Code à copier"
    {% raw %}
    {{ IDE('scripts/fct_carre') }}

    {{ figure() }}
    {% endraw %}
    ```

{{ IDE('scripts/fct_carre') }}

{{ figure() }}

!!! info "Fichier utilisés pour cet exemple"

    ```python title="fct_carre.py"
    # --- PYODIDE:env --- #

    import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
    plt = PyodidePlot()  # Disponible après l'import de matplotlib

    # --- PYODIDE:code --- #

    xs = [-3 + k * 0.1 for k in range(61)]
    ys = [x**2 for x in xs]
    plt.plot(xs, ys, "r-")
    plt.grid()  # Optionnel : pour voir le quadrillage
    plt.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt.title("La fonction carré")
    plt.show()
    ```

### Tracé de deux graphiques dans deux fenêtres séparées

!!! info ""
    
    ```markdown title="Code à copier"
    {% raw %}
    {{ IDE('scripts/fct_carre_cube') }}
 
    {{ figure('cible_1') }}

    {{ figure('cible_2') }}
    {% endraw %}
    ```

{{ IDE('scripts/fct_carre_cube') }}
 
{{ figure('cible_1') }}

{{ figure('cible_2') }}

!!! info "Fichier utilisés pour cet exemple"

    ```python title="fct_carre_cube.py"
    # --- PYODIDE:env --- #

    import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
    plt1 = PyodidePlot('cible_1')  
    plt2 = PyodidePlot('cible_2')

    # --- PYODIDE:code --- #

    xs = [-3 + k * 0.1 for k in range(61)]
    ys = [x**2 for x in xs]
    plt1.plot(xs, ys, "r-")
    plt1.grid()  # Optionnel : pour voir le quadrillage
    plt1.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt1.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt1.title("La fonction carré")
    plt1.show()

    xs = [-2 + k * 0.1 for k in range(41)]
    ys = [x**3 for x in xs]
    plt2.plot(xs, ys, "r-")
    plt2.grid()  # Optionnel : pour voir le quadrillage
    plt2.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt2.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt2.title("La fonction cube")
    plt2.show()
    ```

### Tracé de deux graphiques superposés dans la même fenêtre

!!! info ""
    
    ```markdown title="Code à copier"
    {% raw %}
    {{ IDE('scripts/carre_cube_superposees') }}

    {{ figure('cible_double') }}
    {% endraw %}
    ```

{{ IDE('scripts/carre_cube_superposees') }}

{{ figure('cible_double') }}
    
!!! info "Fichier utilisés pour cet exemple"

    ```python title="carre_cube_superposees.py"
    # --- PYODIDE:env --- #

    import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
    plt = PyodidePlot('cible_double')  


    # --- PYODIDE:code --- #

    xs1 = [-3 + k * 0.1 for k in range(61)]
    ys1 = [x**2 for x in xs1]

    xs2 = [-2 + k * 0.1 for k in range(41)]
    ys2 = [x**3 for x in xs2]

    plt.plot(xs1, ys1, "r-", xs2, ys2, "b+")  
    plt.grid()  # Optionnel : pour voir le quadrillage
    plt.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt.title("La fonction carré et la fonction cube")
    plt.show()
    ```

!!! info "Documentation détaillée"

    Toutes les possibilités n'ont pas été présentées ici.

    [Documentation officielle pour l'utilisation de Matplotlib](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/matplotlib/){ .md-button target="_blank" rel="noopener" } 


!!! danger "Attention"

    Il ne faut pas utiliser plusieurs fois le même identifiant dans une page du site, par exemple ici l'identifiant `cible_1`
    ```markdown title=""
    {% raw %}
    {{ figure('cible_1') }}
    {% endraw %}
    ``` 

    De même, on ne peut utiliser qu'une seule fois dans une page :
    ```markdown title=""
    {% raw %}
    {{ figure() }}
    {% endraw %}
    ``` 

## II. Utiliser la tortue par Romain Janvier

!!! danger "Pour pouvoir utiliser la bibliothèque turtle"

    Si votre site a été créé à partir du site modèle de ce tutoriel, tout fonctionnera correctement.

    ??? note "A faire si vous avez réalisé une mise à jour d'un ancien site vers Pyodide Mkdocs Theme"

        * Il faut rajouter dans le dossier `mkdocs.yml` : 
        ```yaml 
        plugins:
          - pyodide_macros:
              build:
                python_libs:
                  - turtle
        ```

        * Il faut mettre le dossier `turtle` à la racine du site :  [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/turtle.zip)
        Décompresser le fichier `turtle.zip` puis mettre le dossier `turtle` à la racine du projet.

        ![turtle](telecharger_turtle.png){ width=50% }

        Vous obtiendrez : 

        ![turtle à la racine](ajout_turtle.PNG){ width=20% }


```markdown title="Le code à copier"
???+ question "Utilisation de la tortue"

    {% raw %}
    {{ IDE('scripts/arbre_tortue') }}
    {% endraw %}
    
    <div id="cible_3" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>
```


???+ question "Utilisation de la tortue"

    {{ IDE('scripts/arbre_tortue') }}
    
    <div id="cible_3" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

??? note "Fichier utilisé pour cet exemple"

    Il faut bien **adapter** la section `# --- PYODIDE:post --- #` avec le nom de l'id utilisé dans le div. (ici `"cible_3"`)

    ```python title="arbre_tortue.py"
    # --- PYODIDE:env --- #
    from js import document
    if "restart" in globals():
        restart()

    # --- PYODIDE:code --- #
    from turtle import *
    speed(10)

    def arbre(l=100, n=5):
        forward(l)
        if n > 0:
            left(45)
            arbre(l/2, n-1)
            right(90)
            arbre(l/2, n-1)
            left(45)
        back(l)

    arbre(200, 5)
    # --- PYODIDE:post --- #
    done()
    document.getElementById("cible_3").innerHTML = svg()   
    ```

*Crédit pour la réalisation de l'utilisation de  turtle : Romain Janvier*


