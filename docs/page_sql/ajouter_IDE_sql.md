---
author: Mireille Coilhac
title: Des pages avec SQL
---

!!! danger "Attention"

    Page en construction

Des exemples sur le site modèle :  [Page avec SQL du site modèle](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/avec_SQL/exercices_sql/){:target="_blank" }


!!! info "La structure du site"


    Dans le fichier `mkdocs.yml` vers la ligne 125, dans plugins il doit y avoir `- sqlite-console` absolument après : `- pyodide_macros`

    ![Sous plugins](plugins.png){ width=50% }

    * Dans le fichier `.gitlab-ci.yml` environ à partir de la ligne 15, modifier si nécessaire pour avoir ceci :
    ```markdown title=""
    build:
    stage: build
    before_script:
        - python -m venv .venv
        - source .venv/bin/activate
        - pip install --upgrade pip
        - pip install -r requirements.txt
    ```

    * Dans le fichier `requirements.txt` ajouter la dernière ligne

    ```markdown title="Fichier requirements.txt"
    pyodide-mkdocs-theme
    mkdocs-awesome-pages-plugin
    mkdocs-enumerate-headings-plugin
    mkdocs-exclude-search
    mkdocs-sqlite-console
    ```
    

!!! info "Syntaxe pour utiliser un IDE avec SQL"

    Comme précisé dans la  [documentation](https://epithumia.github.io/mkdocs-sqlite-console/usage/) , il faut utiliser la syntaxe suivante: 
    ```markdown title=""
    {% raw %}
    {!{ sqlide titre="essai" espace="avec_sql"}!}
    {% endraw %}
    ```

!!! info "Chemins"

    Ne pas oublier aussi de prendre les chemins relatifs depuis le dossier `docs` pour les fichiers init, sql ou base.

[Documentation officielle](https://epithumia.github.io/mkdocs-sqlite-console/usage/){:target="_blank" }