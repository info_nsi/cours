# --- PYODIDE:code --- #

# un tableau cents en compréhension qui contient 10 entiers 100.
cents = ...

# un tableau entiers en compréhension qui contient les 10 entiers entre 1 et 10 compris.
entiers = ...

# --- PYODIDE:corr --- #

cents = [100 for k in range(10)]
entiers = [k for k in range(1, 11)]


# --- PYODIDE:tests --- #

assert cents == [100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
assert entiers == [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# --- PYODIDE:secrets --- #

assert cents == 10 * [100]
assert entiers == [k for k in range(1, 11)]

