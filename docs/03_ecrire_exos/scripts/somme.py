# --------- PYODIDE:code --------- #

def somme(nombres):
    ...


# --------- PYODIDE:corr --------- #

def somme(nombres):
    resultat = 0
    for nbre in nombres:
        resultat = resultat + nbre
    return resultat


# --------- PYODIDE:tests --------- #

assert somme([]) == 0
assert somme([1, 2, 3]) == 6
assert somme([5, 6, 7]) == 18


# --------- PYODIDE:secrets --------- #

assert somme([5, 6, 7, 0, 100]) == 118
