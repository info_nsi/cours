# --------- PYODIDE:code --------- #

def dernier(ma_liste):
    ...


# --------- PYODIDE:corr --------- #

def dernier(ma_liste):
    return ma_liste[len(ma_liste) - 1]


# --------- PYODIDE:tests --------- #

assert dernier([1]) == 1
assert dernier([3, 4, 5]) == 5


# --------- PYODIDE:secrets --------- #

assert dernier([5, 4, 3]) == 3
