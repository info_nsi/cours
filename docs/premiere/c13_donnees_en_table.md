---
title: Les données en table
---

## 1) Les données en table

Le stockage de l'information est un enjeu majeur pour nos sociétés. Les données peuvent être stockées sous forme de tableau, on parle alors de données tabulaires ou plus simplement de données en table.

Voici un exemple de données stockées sous forme de tableau :

| nom | prénom | date_naissance |
| --- | --- | --- |
| Durand | Jean-Pierre | 23/05/1985 |
| Dupont | Christophe | 15/12/1967 |
| Terta | Henry | 12/06/1978 |

## 2) le format CSV

### a) introduction

Le format CSV permet de stocker des données sous forme de tableau, d'après Wikipédia :

*Un fichier CSV est un fichier texte, par opposition aux formats dits « binaires ». Chaque ligne du texte correspond à une ligne du tableau et les virgules correspondent aux séparations entre les colonnes. Les portions de texte séparées par une virgule correspondent ainsi aux contenus des cellules du tableau.*

Voici le tableau ci-dessus au format CSV :

```
nom,prenom,date_naissance
Durand,Jean-Pierre,23/05/1985
Dupont,Christophe,15/12/1967
Terta,Henry,12/06/1978
```
### b) descripteurs

*nom*, *prenom* et *date_naissance* sont appelés des descripteurs. Les descripteurs permettent de décrire un *objet* et ainsi de pouvoir le distinguer d'un autre *objet* du même type (*nom*, *prenom* et *date_naissance* sont des caractéristiques qui permettent de distinguer 2 personnes). "Durand", "Dupont" et "Terta" sont les valeurs du descripteur *nom*.

### c) séparateur

La virgule est un standard pour les données anglo-saxonnes, mais pas pour les données aux normes françaises. En effet, en français, la virgule est le séparateur des chiffres décimaux. Il serait impossible de différencier les virgules des décimaux et les virgules de séparation des informations. C’est pourquoi on utilise un autre séparateur : le point-virgule (;). Dans certains cas cela peut engendrer quelques problèmes, vous devrez donc rester vigilants sur le type de séparateur utilisé.

Les tableurs, tels que "Calc" (Libre Office), sont normalement capables de lire les fichiers au format CSV. J'ai précisé "normalement" car certains tableurs gèrent mal le séparateur CSV "point-virgule" et le séparateur des chiffres décimaux "virgule".

## 3) traitement des données

Pour traiter des données, nous allons utiliser les bibliothèques (modules) Python csv et Pandas.
On rappelle qu'une bibliothèque  permet de rajouter des fonctionnalités par rapport au langage de base. La bibliothèque Pandas est donc très utilisée pour tout ce qui touche au traitement des données.

Nous allons nous intéresser à cette bibliothèque dans la partie *activité* de ce chapitre.


## 4) Pour s'entrainer
Voici deux fichiers :

[ident_virgule.csv](./file/ident_virgule.csv)

[villes_virgule.csv](./file/villes_virgule.csv)

[les-arbres.csv](./file/les-arbres.csv)


Répondre aux questions suivantes:

- Quel est le nombre de villes de cette table ?

??? tip "Solution"
	36700

- Combien de villes contiennent Paris ou paris dans leur nom ?

??? tip "Solution"
	10
- Combien de villes sont vides en habitant en 2012 ?

??? tip "Solution"
	926

- Quel est le nom de la ville qui a sur son territoire le sommet de la france ? Quelle l'altitude de ce sommet ?

??? tip "Solution" 
	Chamonix-Mont-Blanc à 4807m

- Quel est le nom de la ville qui possède le point le plus bas de France ? 

??? tip "Solution"
	Quimper, altitude min -5

- Quelle est la ville qui a la plus grande densité ?

??? tip "Solution"
	Levallois-Perret


- Combien y-a-t-il de villes dans les Haut de Seine (départements 92) ?

??? tip "Solution"
	35

- Quel est le département qui contient le plus de communes ? (cette question est plus difficile, nous avons besoin de construire un dictionnaire avec tous les départements en clés)

??? tip "Solution"
	62, Pas-de-calais

- Bonus : Quelle est la circonscription départementale la plus vaste (en superficie) ?

??? tip "Soltuion"
	La Guyanne avec 85 531 km2
-
