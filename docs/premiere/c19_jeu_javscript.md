---
title: jeu interactif javascript
---

## Atelier de programmation web : Création d'un jeu interactif

Les étapes suivantes te guideront progressivement, pour créer un petit jeu complet.

## Objectifs
1. Maitriser le structure d'une page web HTML avec les éléments input et button.
2. Appliquer des styles visuels avec CSS.
3. Ajouter des interactions dynamiques grâce à JavaScript.

---

## Fichiers de départ
### Fichier HTML minimal de départ
```html
<!DOCTYPE html>
<html>
<head>
    <title>Jeu interactif</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <h1>Bienvenue sur cette page</h1>
    <div id="contenu"></div>
    <div id="jeu"></div>
    <script src="script.js"></script>
</body>
</html>
```

### Fichier CSS minimal de départ
```css
/* Fichier vide pour l'instant */
```

### Fichier JavaScript minimal de départ
```javascript
// Fichier vide pour l'instant
```

---

## Étapes du projet

### Étape 1 : Créer la structure HTML de base
- Ajoutez une balise `<h1>` pour afficher un titre : "Bienvenue sur cette page".
- Créez une `<div>` vide avec l'attribut `id="contenu"` pour contenir l'introduction.
- Ajoutez une autre `<div>` vide avec l'attribut `id="jeu"` pour afficher le jeu.

### Étape 2 : Appliquer des styles avec CSS
- Ajoutez des styles pour :
  - Centrer les titres `<h1>` et `<h2>`.
  - Définir une couleur de fond pour le corps de la page (`body`).
  - Styliser la `<div id="contenu">` avec une taille de police agrandie et une marge.

Exemple :
```css
body {
    background-color: lightsalmon;
}

h1, h2 {
    text-align: center;
}

#contenu {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 2em;
    margin: 1em;
}
```

### Étape 3 : Ajouter un champ "Prénom" et un bouton dans le HTML
- Dans la `<div id="contenu">`, ajoutez :
  - Un champ de saisie (`<input>`) pour le prénom avec l'attribut `id="prenom"`.
  - Un bouton avec un texte "OK" qui appellera la fonction JavaScript `bonjour()` lorsqu'il sera cliqué.


### Étape 4 : Créer la fonction `bonjour()` en JavaScript
- Dans le fichier `script.js`, créez une fonction `bonjour()` :
  - Récupérez la valeur du champ prénom.
  - Affichez un message personnalisé dans un `<h2>` avec une proposition de jeu.
  - Ajoutez deux boutons : "Oui" (qui lance le jeu avec `lance_jeu()`) et "Non" (qui appelle `au_revoir()`).

Exemple :
```javascript
function bonjour(){
    let prenom = document.getElementById("prenom").value;
    console.log(prenom);
    const bonjour = document.getElementById("bonjour_et_proposition");
    bonjour.innerHTML = "<h2>Bonjour " + prenom + "</h2><p>Veux tu jouer à un jeu</p><button onclick='lance_jeu()'>Oui\
    </button><button onclick='au_revoir()'>Non, merci</button>";
}
```

### Étape 5 : Créer la fonction `au_revoir()`
- Dans le fichier `script.js`, ajoutez une fonction `au_revoir()` :
  - Modifiez le contenu de la page pour afficher "Au revoir" et un message de remerciement.

Exemple :
```javascript
function au_revoir() {
	document.body.innerHTML = `<h2>... A compléter
}
```

### Étape 6 : Modifier le CSS pour créer des cases du jeu
- Ajoutez une classe `.case` dans le fichier CSS :
  - Chaque case doit avoir une largeur et hauteur de 150px.
  - Les cases doivent être affichées en ligne avec `display: inline-block`.

Exemple :
```css
.case {
    margin: 5px;
    width: 150px;
    height: 150px;
    display: inline-block;
    background-color: lightgray;
}
```

### Étape 7 : Créer la fonction `lance_jeu()`
- Utilisez `setInterval()` pour appeler une fonction `myTimer()` toutes les 500 ms.

Exemple :
```javascript
function lance_jeu() {
    setInterval(myTimer, 500);
}
```

### Étape 8 : Créer la fonction `myTimer()`
- Affichez une grille 3x3 avec des cases en HTML.
- Dans une case aléatoire, affichez une image (ex. une souris).

Exemple :
```javascript
function myTimer() {
    document.getElementById("jeu").innerHTML = `
        <div id='l1'>
            <div class='case'></div><div class='case'></div><div class='case'></div>
        </div>
        <div id='l2'>
            <div class='case'></div><div class='case'></div><div class='case'></div>
        </div>
        <div id='l3'>
            <div class='case'></div><div class='case'></div><div class='case'></div>
        </div>`;


	/* Essaie de retrouver le code suivant après l'avoir lu*/
	let cellule = document.getElementsByClassName('case');
        cellule[pos % 9].innerHTML = "<img src='souris.png' onclick='touche()'>";
        pos = ((pos + 1) * 13 % 253);
}
```

[Enregistre souris.png](/files/souris.png)

### Étape 9 : Ajouter la fonction `touche()`
- Ajoutez une fonction pour incrémenter un score chaque fois que l'image est cliquée.

Exemple :
```javascript
let score = 0;
function touche() {
	...
}
```

---

Et maintenant, est-ce que le jeu fonctionne ? Si oui, Bravo !

