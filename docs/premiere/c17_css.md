---
title: le style avec CSS
---

## Les Sélecteurs CSS

Avant de plonger dans les propriétés, il est important de comprendre les sélecteurs CSS :

- **Sélecteur universel (`*`)** : s'applique à tous les éléments.
  ```css
  * {
    margin: 0;
    padding: 0;
  }
  ```
- **Sélecteur de type (`element`)** : cible un type d'élément HTML.
  ```css
  p {
    color: blue;
  }
  ```
- **Sélecteur de classe (`.nom_de_classe`)** : cible les éléments ayant une classe spécifique.
  ```css
  .conteneur {
    padding: 20px;
  }
  ```
- **Sélecteur d'ID (`#nom_id`)** : cible un élément ayant un ID unique.
  ```css
  #titre {
    font-size: 24px;
  }
  ```
- **Sélecteurs combinés** : permettent de combiner des sélecteurs pour cibler des éléments spécifiques.
  ```css
  div.article {
    margin-bottom: 10px;
  }
  ```


# Proriétés de style

Voici une présentation de 10 propriétés CSS importantes pour styliser des pages web. Ces propriétés sont simples à utiliser et permettent de mieux comprendre comment fonctionne le CSS.

### 1. **color**
Définit la couleur du texte.
```css
p {
  color: blue;
}
```

### 2. **background-color**
Définit la couleur de fond d'un élément.
```css
div {
  background-color: lightgray;
}
```

### 3. **font-size**
Change la taille du texte.
```css
h1 {
  font-size: 24px;
}
```


### 4. **font-family**
Change la famille de police : serif, sans-serif, cursive...
```css
h2 {
  font-family: cursive;
}
```

### 5. **margin**
Ajoute un espace extérieur autour d'un élément.
```css
p {
  margin: 20px;
}
```

### 6. **padding**
Ajoute un espace intérieur entre le contenu et les bords d'un élément.
```css
div {
  padding: 10px;
}
```

### 7. **border**
Ajoute une bordure autour d'un élément.
```css
img {
  border: 2px solid black;
}
```

### 8. **width** et **height**
Définissent la largeur et la hauteur d'un élément.
```css
div {
  width: 200px;
  height: 100px;
}
```

### 9. **text-align**
Définit l'alignement horizontal du texte.
```css
h1 {
  text-align: center;
}
```

### 10. **display**
Contrôle la manière dont un élément est affiché.
```css
span {
  display: inline;
}
div {
  display: block;
}
```

---

### Conseils pour aller plus loin :
- Combinez plusieurs propriétés pour créer des styles plus complexes.
- Consultez la documentation CSS sur [MDN Web Docs](https://developer.mozilla.org/fr/docs/Web/CSS) pour approfondir vos connaissances.

