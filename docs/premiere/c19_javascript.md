---
title: Quelques bases de Javascript
---


Reprenons le code de la page des formulaires

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Exemple Javascript</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
            padding: 20px;
        }
    </style>
</head>
<body>
    <h1 id="titre">Javascript</h1>

    <form>
        <label for="nom">Votre nom :</label><br>
        <input type="text" id="nom" name="nom"><br><br>

        <p>Choisissez une couleur :</p>
        <label>
            <input type="radio" name="couleur" value="lightblue" onclick="changerCouleur()">
            Bleu clair
        </label><br>
        <label>
            <input type="radio" name="couleur" value="lightgreen" onclick="changerCouleur()">
            Vert clair
        </label><br>
        <label>
            <input type="radio" name="couleur" value="lightyellow" onclick="changerCouleur()">
            Jaune clair
        </label><br><br>

        <label>
            <input type="checkbox" name="newsletter">
            S'inscrire à la newsletter
        </label><br><br>

        <button type="submit">Envoyer</button>
    </form><br>
        <button  onclick="changeTitre()">Change titre</button>
        <p>a</p>
        <p>a</p>
</body>
<script>
        function changerCouleur() {
            // Récupérer les radios
            const radios = document.getElementsByName('couleur');
            let couleur = 'white'; // Valeur par défaut

            // Vérifier quelle radio est sélectionnée
            for (const radio of radios) {
                if (radio.checked) {
                    couleur = radio.value;
                    break;
                }
            }


            // Changer la couleur de fond
            document.body.style.backgroundColor = couleur;
        }
        function changeTitre(){
                let titre = document.getElementById("titre");
                titre.textContent = "Bonjour, le monde !";
        }
        </script>

</html>                                                                           
```

Ajoute quelques paragraphes et éxécute le code suivant :

let paragraphes = document.getElementsByTagName("p");

for (paragraohe of paragraphes) { paragraphe.textContent = "texte vide";}

Exemple de page html pour cet exercice

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    
    <title>Exemple Javascript</title>
</head>
<body>
<p> Premier paragraphe</p>
<p> Deuxième paragraphe</p>

<button onclick="efface()">Effacer</button>

</body>
<script>
function efface(){

}
// ... A compléter
</script>
</html>
```
