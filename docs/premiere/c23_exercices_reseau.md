---
title: Exercices corrigés d’adressage IP
---

## Exercice n°1 : Masque de sous-réseau

Complétez le tableau suivant en déterminant le masque de réseau correspondant à chaque notation CIDR :

| Nombre de bits du masque | Masque |
|--------------------------|--------|
| 10                       |        |
| 23                       |        |
| 30                       |        |
| 13                       |        |
| 19                       |        |
| 21                       |        |

??? tip "Solution"

	| Nombre de bits du masque | Masque |
	|--------------------------|--------------|
	| 10                       | 255.192.0.0  |
	| 23                       | 255.255.254.0 |
	| 30                       | 255.255.255.252 |
	| 13                       | 255.248.0.0 |
	| 19                       | 255.255.224.0 |
	| 21                       | 255.255.248.0 |



## Exercice n°2 : Calcul de l’adresse réseau

Complétez le tableau suivant en déterminant l’adresse réseau correspondant à chaque adresse IP donnée :

| @IP                   | @Réseau |
|----------------------|---------|
| 166.13.21.2/20       |         |
| 12.66.34.2/10        |         |
| 220.220.220.35/28    |         |
| 193.167.1.56/27      |         |
| 173.16.100.3/18      |         |

??? tip "Solution"

	### Méthode de calcul

	Exemple pour `166.13.21.2/20` :

	1. Convertir l’adresse IP en binaire :

	   - `166.13.21.2 = 10100110.00001101.00010101.00000010`

	2. Effectuer un **ET logique** avec le masque `/20` :

	   - `/20 = 11111111.11111111.11110000.00000000`

	3. Résultat de l’opération :

	   - `10100110.00001101.00010000.00000000` = **166.13.16.0/20**

	| @IP                   | @Réseau         |
	|----------------------|----------------|
	| 166.13.21.2/20       | 166.13.16.0/20  |
	| 12.66.34.2/10        | 12.64.0.0/10    |
	| 220.220.220.35/28    | 220.220.220.32  |
	| 193.167.1.56/27      | 193.167.1.32    |
	| 173.16.100.3/18      | 173.16.64.0     |



## Exercice n°3 : Informations sur les sous-réseaux

Complétez le tableau suivant en calculant les différentes informations pour chaque adresse réseau donnée :

| Adresse réseau      | Masque  | Première @IP | Dernière @IP | Diffusion |
|---------------------|--------|-------------|--------------|-----------|
| 145.16.64.0 /18    |        |             |              |           |
| 192.168.1.32 /27   |        |             |              |           |
| 200.168.1.0 /28    |        |             |              |           |
| 18.1.5.24 /29      |        |             |              |           |
| 145.16.64.0 /20    |        |             |              |           |
| 10.96.0.0 /11      |        |             |              |           |

??? tip "Solution"

	| Adresse réseau      | Masque           | Première @IP      | Dernière @IP       | Diffusion         |
	|---------------------|----------------|-----------------|------------------|------------------|
	| 145.16.64.0 /18    | 255.255.192.0   | 145.16.64.1     | 145.16.127.254   | 145.16.127.255   |
	| 192.168.1.32 /27   | 255.255.255.224 | 192.168.1.33    | 192.168.1.62     | 192.168.1.63     |
	| 200.168.1.0 /28    | 255.255.255.240 | 200.168.1.1     | 200.168.1.14     | 200.168.1.15     |
	| 18.1.5.24 /29      | 255.255.255.248 | 18.1.5.25       | 18.1.5.30        | 18.1.5.31        |
	| 145.16.64.0 /20    | 255.255.240.0   | 145.16.64.1     | 145.16.79.254    | 145.16.79.255    |
	| 10.96.0.0 /11      | 255.224.0.0     | 10.96.0.1       | 10.127.255.254   | 10.127.255.255   |


