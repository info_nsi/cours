---
title: Formulaire en HTML
---

Voici un exemple de formulaire HTML

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Exemple de formulaire</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
            padding: 20px;
        }
    </style>
    <script>
        function changerCouleur() {
            // Récupérer les radios
            const radios = document.getElementsByName('couleur');
            let couleur = 'white'; // Valeur par défaut

            // Vérifier quelle radio est sélectionnée
            for (const radio of radios) {
                if (radio.checked) {
                    couleur = radio.value;
                    break;
                }
            }

            // Changer la couleur de fond
            document.body.style.backgroundColor = couleur;
        }
    </script>
</head>
<body>
    <h1>Formulaire de test</h1>

    <form>
        <label for="nom">Votre nom :</label><br>
        <input type="text" id="nom" name="nom"><br><br>

        <p>Choisissez une couleur :</p>
        <label>
            <input type="radio" name="couleur" value="lightblue" onclick="changerCouleur()">
            Bleu clair
        </label><br>
        <label>
            <input type="radio" name="couleur" value="lightgreen" onclick="changerCouleur()">
            Vert clair
        </label><br>
        <label>
            <input type="radio" name="couleur" value="lightyellow" onclick="changerCouleur()">
            Jaune clair
        </label><br><br>

        <label>
            <input type="checkbox" name="newsletter">
            S'inscrire à la newsletter
        </label><br><br>

        <button type="submit">Envoyer</button>
    </form>
</body>
</html>
```
