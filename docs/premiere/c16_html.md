---
title: Syntaxe élémentaire HTML
---

Ce mémo présente une dizaine de balises essentielles pour écrire un document HTML. Ces balises constituent les bases de la structure et du contenu d'une page web.

## Structure de base d'un document HTML

```html
<!DOCTYPE html>
<html>
<head>
    <title>Titre de la page</title>
</head>
<body>
    <!-- Contenu principal ici -->
</body>
</html>
```

---

## Les balises essentielles

### 1. `<html>`
- Définit le début et la fin d'un document HTML.
- Exemple :

```html
<html>
    <!-- Contenu HTML -->
</html>
```

### 2. `<head>`
- Contient des métadonnées et des liens vers des ressources externes comme des styles ou des scripts.
- Exemple :

```html
<head>
    <meta charset="UTF-8">
    <title>Exemple</title>
</head>
```

### 3. `<body>`
- Contient tout le contenu visible de la page.
- Exemple :

```html
<body>
    <p>Bienvenue sur ma page !</p>
</body>
```

### 4. `<h1>` à `<h6>`
- Définissent les titres, du plus important `<h1>` au moins important `<h6>`.
- Exemple :

```html
<h1>Titre principal</h1>
<h2>Sous-titre</h2>
```

### 5. `<p>`
- Définit un paragraphe de texte.
- Exemple :

```html
<p>Ceci est un paragraphe de texte.</p>
```

### 6. `<a>`
- Crée un lien hypertexte.
- Exemple :

```html
<a href="https://www.example.com">Visitez Example</a>
```

### 7. `<img>`
- Affiche une image.
- Attributs courants : `src` (source de l'image) et `alt` (texte alternatif).
- Exemple :

```html
<img src="image.jpg" alt="Description de l'image">
```

### 8. `<ul>`, `<ol>` et `<li>`
- Créent des listes. `<ul>` pour les listes non ordonnées, `<ol>` pour les listes ordonnées, et `<li>` pour chaque élément.
- Exemple :

```html
<ul>
    <li>Élément 1</li>
    <li>Élément 2</li>
</ul>

<ol>
    <li>Première étape</li>
    <li>Deuxième étape</li>
</ol>
```

### 9. `<div>`
- Sert à regrouper des éléments pour des mises en page ou styles spécifiques.
- Exemple :

```html
<div>
    <p>Bloc de contenu</p>
</div>
```

### 10. `<span>`
- Sert à styliser une portion spécifique de texte en ligne.
- Exemple :

```html
<p>Texte avec un <span style="color: red;">mot rouge</span>.</p>
```

---

## Ressources supplémentaires
- [Documentation MDN](https://developer.mozilla.org/fr/docs/Web/HTML)
- [Tutoriels W3Schools](https://www.w3schools.com/html/)

