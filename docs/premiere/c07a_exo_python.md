---
title: Exercices Pyhton
---

### activité 1.1

Soit la fonction suivante :

```python
def ma_fct(a,b):
	if a < 5 and b > 2 :
		return 42
	else :
		return 24

val = ma_fct(6, 3)
```

Quelle est la valeur de la variable  *val* après l'exécution de ce programme ?

Vérifiez votre réponse à l'aide  de la console.

### activité 1.2

Soit la fonction suivante :

```python
def ma_fct(a,b):
	if a < 5 or b > 2 :
		return 42
	else :
		return 24

val = ma_fct(6, 3)
```

Quelle est la valeur de la variable  *val* après l'exécution de ce programme ?

Vérifiez votre réponse à l'aide  de la console.

### activité 1.3

Soit la fonction suivante :

```python
def ma_fct(a,b):
	if a < 2 or b < 2 :
		return 42
	else :
		return 24

val = ma_fct(6, 3)
```

Quelle est la valeur de la variable  *val* après l'exécution de ce programme ?

Vérifiez votre réponse à l'aide  de la console.

### activité 1.4

Soit la fonction suivante :

```python
def ma_fct(a):
	b = 0
	while a > 2:
		b = b + 1
		a = a - 2
	return b
		
val = ma_fct(6)
```

Quelle est la valeur de la variable  *val* après l'exécution de ce programme ?

Vérifiez votre réponse à l'aide  de la console.


### activité 1.5

Soit la fonction suivante :

```python
def ma_fct(a):
	b = 3
	while a > 0:
		b = b + a
		a = a - 2
	return b
		
val = ma_fct(6)
```

Quelle est la valeur de la variable  *val* après l'exécution de ce programme ?

Vérifiez votre réponse à l'aide  de la console.

### activité 1.6

On désire programmer une fonction qui prend en paramètre  le rayon d'un cercle et renvoie son aire :

```python
import math

def aire_cercle(...):
	aire = math.pi*r**2
	return ...
```

Complétez la fonction *aire_cercle* ci-dessus (remplacez les ...), puis écrivez la documentation de cette fonction sous forme de docstring.

### activité 1.7

On désire programmer une fonction qui prend en paramètre un nombre et qui renvoie la chaîne de caractères "pair" si le nombre est pair et "impair" dans le cas contraire 

```python
import math

def pair_impair(n):
	if ... % 2 == 0:
		return ...
	else :
		return "impair"
```

Complétez la fonction *pair_impair* ci-dessus (remplacez les ...), puis écrivez la documentation de cette fonction sous forme de docstring.

### activité 1.8

On désire écrire une fonction *rebours* qui permet d'afficher un compte à rebours à l'écran. Cette fonction prend en paramètre la valeur de départ.

Exemple  : si on  tape dans la console *rebours(5)*, on doit obtenir :

```
5
4
3
2
1
0
```
 
Complétez la fonction *rebours* suivante :

```python
def rebours(n):
	while ...:
		print(n)
		...
```

### activité 1.9

Vous êtes gérant d'un magasin et vous désirez écrire un programme Python qui calculera automatiquement le montant de la facture des clients.

Tout client qui achète au moins 5 fois le même article se voit octroyer une remise de 5 % (uniquement sur le montant de l'achat de cet article). 

Afin de simplifier le problème, on considère qu'un client n'achète qu'un seul type d'article.

Écrivez une fonction *facture* qui prend en paramètre le prix unitaire de l'article et le nombre d'articles achetés. Cette fonction doit renvoyer le montant de la facture.

### activité 1.10

Vous allez créer "un générateur automatique de punition" :

Écrivez une fonction *punition* qui prendra 2 paramètres : une chaîne de caractère et un nombre entier

Par exemple :

Si on passe comme paramètres à notre fonction : "Je ne dois pas discuter en classe" et 3

La fonction devra permettre d'afficher :

Je ne dois pas discuter en classe

Je ne dois pas discuter en classe

Je ne dois pas discuter en classe

### activité 1.11

Écrivez une fonction *multi* permettant d'afficher une table de multiplication. Cette fonction devra prendre en paramètre la table désirée.

Par exemple si l'on passe le paramètre 3 à la fonction, la fonction devra permettre d'afficher :

```
1 x 3 = 3

2 x 3 = 6

...

...

10 x 3 = 30
```
