---
author: Mireille Coilhac
title: Migration depuis la forge AEIF
---

!!! danger "Attention : Migration à programmer de la forge AEIF vers la forge Education Nationale"

    La forge AEIF va très bientôt disparaître. Elle est remplacée depuis le 29 mars par la Forge des communs numériques éducatifs qui rejoint les services partagés de Apps Education à l'adresse suivante : 
    [Forge apps.eduction](https://forge.apps.education.fr){ .md-button target="_blank" rel="noopener" }   
    
      
    Nous vous invitons à y migrer vos projets qui étaient sur la forge AEIF avant le 31 juin 2024. Un tutoriel est disponible à cet effet. Pour la redirection, si vous avez un site réalisé avec ce tuto, il suffit de lire l'encadré suivant, plus simple que ce qui est indiqué dans le tuto en lien. 
    [Tutoriel de migration](https://docs.forge.apps.education.fr/migration/migration_projet/){ .md-button target="_blank" rel="noopener" }

    

!!! danger "Redirection après migration"

    ??? note "Dans le cas où votre site n'est pas réalisé avec Pyodide Mkdocs Theme"

        Pour rediriger votre site de la forge AEIF vers ce même site une fois migré sur la forge Education Nationale, il suffit dans le dossier `my_theme_customizations`m d'ouvrir le fichier `main.html`. Au début du fichier, il faut ajouter la dernière ligne du bloc ci-dessous : 

        ````html
        {% raw %}
        {% extends "base.html" %}

        {% block content %}
        {{super()}}
        <script src="{{ base_url }}/pyodide-mkdocs/ide.js"></script> 
        {% endblock %}

        {% block extrahead %}
        <meta http-equiv="refresh" content="0;url={{ [config.site_url,page.url] | join('/') | replace('aeif','apps.education') }}">
        {% endraw %}
        ````

        ````html title="Ligne à recopier et à ajouter"
        {% raw %}
        <meta http-equiv="refresh" content="0;url={{ [config.site_url,page.url] | join('/') | replace('aeif','apps.education') }}">
        {% endraw %}
        ````  

        ⌛ Il faudra patienter quelques minutes avant que la redirection ne soit effective.


    ??? note "Dans le cas où votre site est réalisé avec Pyodide Mkdocs Theme"

        Vous devez voir en bas à droite de votre site : ![version du thème](../maj/images/version_theme.png){ width=15% }

        Lire la fin de ce tutoriel : [Tutoriel de migration](https://docs.forge.apps.education.fr/migration/migration_projet/){ .md-button target="_blank" rel="noopener" }

        
