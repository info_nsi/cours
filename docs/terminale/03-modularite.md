---
author: Pascal Haslé
title: Modularité des programmes
---

Les fichiers Python peuvent regrouper toutes les fonctions d'un projet :

``` python
# blocs de fonctions
def moyenne(tab) :
    ...

def recherche(tab,val) :
    ...

# Corps du programme
tableau = [1,2,3]
print( moyenne(tableau) ) # appel de fonction
print( recherche(tableau, 5)) # autre appel de fonction
```
Mais... 

avec le principe de **modularité**, l'idée consiste à séparer ce programme en deux fichiers  :

- un fichier contenant les fonctions : on dit qu'il devient alors un ***module*** ;
- un fichier contenant le corps du programme, et qui charge le module à l'aide d'une instruction `import`.

``` python
# Fichier : Mon_Joli_Module.py
def moyenne(tab) :
    ...

def recherche(tab,val) :
    ...
``` 

``` python 
# Fichier : Mon_Petit_Programme.py
from Mon_Joli_Module import * # sans l'extension .py 

tableau = [1,2,3]
print( moyenne(tableau) ) 
print( recherche(tableau, 5)) 
```

!!! tip "Les intérêts de la modularité des programmes"

    1. Claire **séparation des rôles** :

        - le module est écrit par un programmeur, un développeur ;
        - le corps du programme est de la responsabilité de l'utilisateur du module, qui est peut-être moins expert en informatique que le programmeur du module.

    2. **Réutilisation** : le module peut être utilisé dans d'autres projets, d'autres programmes (sans faire de copier/coller de lignes de code dans un fichier Python). Il devient ainsi une brique logicielle au service d'un projet plus global.


!!! done "Exemple : module Turtle"
    En classe de Première, vous avez utilisé le module Turtle de Python pour faire des dessins à l'aide d'instructions du type `forward(100)` ou `left(90)`.

    Vous ne savez pas quel code permet de dessiner une ligne de 100 pixels de long quand vous écrivez `forward(100)` : tout ce qui vous importe, c'est que cela dessine effectivement une ligne.


> Ce qu'on appelle ___module___ dans le langage Python peut être appelée  _bibliothèque logicielle_, _librairie_  (:flag_gb: :flag_us: _library_) ou encore _package_ dans d'autres languages de programmation. Tous les fichiers `.dll` (_Dynamic Link Library_) de Windows® sont ainsi des _modules_.

## ^^2. Interface et implémentation d'un module^^

Afin de faciliter l'utilisation du module, il est souhaitable de lui adjoindre une **interface**. Cette interface donne la liste des fonctions du module et donne un résumé de la **documentation** de chaque fonction (comment et pourquoi les utiliser).

^^Exemple d'une interface :^^

| Fonction | Description |
| :------ | :------- | 
| moyenne(tab) | Renvoie la moyenne des valeurs du tableau|
| recherche(tab,val) | Renvoie True si la valeur val est dans le tableau tab |


!!! tip "Interface d'un côté, implémentation de l'autre"
    Pour chaque module, on distingue donc :

    * son **implémentation** (c'est-à-dire le code lui-même);
    * son **interface**.

    L'interface peut être considérée comme une **abstraction** du module (une description des fonctions du module, mais faite à un niveau assez haut, ignorant les détails concrets de la réalisation), ou encore comme un **contrat** entre l'auteur d'un module et ses utilisateurs.

## ^^3. Une mise en pratique^^

Considérons une situation où une personne dispose d'un fichier `notes.csv` au format CSV contenant toutes ses notes de NSI du trimestre et qu'elle veuille pouvoir obtenir :

- la moyenne de ses notes ;
- sa note maximale ;
- et sa note minimale.

Une équipe est constituée afin de programmer en Python l'outil convenable, avec :

- un&middot;e spécialiste de la gestion des entrées/sorties, qui a écrit le corps de programmme permettant de 

    * lire le fichier CSV et générer un tableau de nombres ;
    * afficher les résultats des calculs de moyenne, maximum et minimum ;

- et vous-même, dont le rôle est d'écrire le __module de traitement des données d'un tableau de nombres__ selon l'__interface__ suivante :

| Fonction | Description |
| :------ | :------- | 
| moyenne(tab) | Renvoie la moyenne des valeurs du tableau tab|
| maximum(tab) | Renvoie la valeur maximale du tableau tab |
| minimum(tab) | Renvoie la valeur minimale du tableau tab |


**Étape 1.** Écrire un module `module_tab.py` qui __implémente__ les trois fonctions de cette __interface__.

**Étape 2.** Télécharger et décompressser le [dossier zippé](https://forge.apps.education.fr/info_nsi/cours/-/raw/main/docs/file/TP_Traitement_Tableau.zip?ref_type=heads){target="_blank"} préparé par le spécialiste des entrées/sorties. Il contient :

- un fichier `notes.csv`, où vous pourrez modifier la liste des notes à traiter ;
- un fichier `traitement_tableau_minimal.py` permettant un affichage en console des résultats du traitement du fichier `notes.csv` ;
- et un fichier `traitement_tableau_avec_boutons.py` qui gère les entrées/sorties à l'aide de boutons.

> Vous pourrez constatez que ces deux fichiers `.py` utilisent le module `module_tab.py` que vous avez écrit : en effet, ils contiennent l'instruction `from module_tab import *`.

**Étape 3.** Après avoir réuni dans un même répertoire votre module `module_tab.py` et les fichiers décompressés, exécuter successivement :

- le programme `traitement_tableau_minimal.py`  ;
- puis le programme `traitement_tableau_avec_boutons.py`.

