---
title: Activités sur les paradigmes de programmation
---

## Programmation fonctionnelle
Complète la fonction suivante qui renvoie `True` si `n` est pair `False` sinon.
```py
def est_pair(n:int)->bool:
    return ....
```

Ensuite écris la fonction `prend_si` qui retourne tous les éléments d'une liste vérifiant la condition.
Teste ta fonction avec `prend_si([1,3,4,5,6,8,9,10], est-pair)`
```py
def prend_si(liste:list, condition)-> list:
    '''
    Retourne la liste des éléments elt de liste tels condition(elt) est True
    Exemple
    >>>prend_si([1,3,4,5,6,8,9,10], est-pair)
    [4,6,8,10]
    '''
	.... code à écrire...
```

## POO
Soit la classe *Personnage* suivante :

```py
class Personnage:
    def __init__(self, nbreDeVie):
        self.vie=nbreDeVie
    def donneEtat (self):
        return self.vie
    def perdVie (self,nbPoint):
        self.vie=self.vie-nbPoint
```
Ajoutez une méthode *soigne* qui permettra d'augmenter l'attribut *vie* d'une  valeur *nbr* (*nbr* sera un paramètre de la méthode *soigne*).

Testez cette méthode en saisissant dans la console Python les instructions suivantes (les unes après les autres) :

- toto = Personnage(15)

- toto.donneEtat()

- toto.perdVie(2)

- toto.soigne(3)

- toto.donneEtat()

### activité 2 POO

Écrivez une classe *Voiture* qui aura un attribut *vitesse* et 3 méthodes :

- une méthode *accelere* qui permettra d'incrémenter l'attribut vitesse d'une unité
- une méthode *freine* qui permettra de diminuer la vitesse
- une méthode *getVitesse* qui renverra la valeur de la vitesse

### activité 3 POO

À partir de la classe créée à l'activité 2 POO, écrivez un programme qui permettra d'atteindre la vitesse de 3 km/h, d'afficher cette vitesse dans la console puis de freiner jusqu'à l'arrêt complet du véhicule.


### activité 4

Cette activité porte sur le problème des Tours de Hanoi. Voici un extrait de l'article Wikipédia consacré aux Tours de Hanoi ([https://fr.wikipedia.org/wiki/Tours_de_Hano%C3%AF](https://fr.wikipedia.org/wiki/Tours_de_Hano%C3%AF)) :

Les tours de Hanoï (originellement, la tour d'Hanoïa) sont un jeu de réflexion imaginé par le mathématicien français Édouard Lucas, et consistant à déplacer des disques de diamètres différents d'une tour de « départ » à une tour d'« arrivée » en passant par une tour « intermédiaire », et ceci en un minimum de coups, tout en respectant les règles suivantes :

- on ne peut déplacer plus d'un disque à la fois ;
- on ne peut placer un disque que sur un autre disque plus grand que lui ou sur un emplacement vide.

On suppose que cette dernière règle est également respectée dans la configuration de départ.

![](img/Tower_of_Hanoi.jpeg)

Dans la suite de cette activité nous allons utiliser la classe Tour donnée ci-dessous :

```python
class Tour:
    def __init__(self, nom, n = 0):
        self.s=[]
        self.nom = nom
        for i in range (n, 0, -1):
            self.s.append(i)
    def empile(self, d):
        assert len(self.s) == 0 or self.s[-1] > d, 'Mouvement interdit'
        self.s.append(d)
        print(f"Ajout du disque {d} sur la tour ", self.nom)
    
    def depile(self):
        assert len(self.s) != 0, 'Impossible, la tour est vide'
        d = self.s.pop()
        print(f"Retire le disque {d} de la tour ",self.nom)
        return d
    
    def affiche(self):
        if len(self.s)==0:
            print(f"la tour {self.nom} est vide")
        else :
            print (f"Tour {self.nom}")
            for d in reversed(self.s):
                print("| ",d," |")
```
##### 1-
Après avoir étudié attentivement la classe Tour, écrivez les instructions Python permettant de créer 3 instances de la classe Tour (une instance permettant de modéliser une tour du jeu). Une tour sera créée avec 2 disques (cette tour sera nommée "A"), les 2 autres tours seront au départ vide (tour "B" et tour "C"). On notera que chaque disque est identifié par un entier (cet entier représente le diamètre du disque correspondant).  

##### 2-
Écrivez une fonction *mouvement*, cette fonction prendra 2 paramètres *t1* et *t2*, tous les deux de type Tour (instance de Tour). Le but de cette fonction est de permettre le passage d'un disque de la tour *t1* vers la tour *t2*.

##### 3-
Écrivez la suite d'instructions permettant de résoudre le jeu avec 2 disques (la tour "A" sera la  tour de "départ", la tour "B" sera la tour "intermédiaire"  et la tour "C" sera la tour "arrivée"). Vous vérifierez votre réponse en affichant le contenu de la tour "C" après l'exécution de votre programme.

##### 4-
Écrivez une fonction récursive *resoudre* permettant de résoudre le jeu "Tours de Hanoi" dans tous les cas (avec n disques). Cette fonction prendra 4 paramètres :  un entier représentant le nombre de disques et trois instances de la classe Tour (représentant la tour "départ", la tour "arrivée" et la tour "intermédiaire").

Deux conseils :

- réfléchissez bien au cas de base de votre fonction récursive
- inspirez-vous de ce que vous avez fait dans la question 3 


