---
title: Correction exercice Bac Blanc décembre
---

#### Partie A

1. (1.0) Pour instancier un objet, il faut appeler le constructeur par le nom de la classe et fournir les arguments:


```python
chien40 = Chien(40,'Duke','wheel dog', 10)
```

2. (1.0) La méthode `changer_role` modifie l'attribut `role`


```python
def changer_role(self, nouveau_role: str):
    self.role = nouveau_role
```

3. (1.0)


```python
chien40.changer_role('leader')
```


#### Partie B

4. (2.0) Plusieurs réponses sont possibles, en créant un nouvelle liste, en retirant avec une recherche de l'indice `ì` et `pop(i)` ou encore avec `remove`


```python
# avec nelle liste
def retirer_chien(self, numero):
    new_liste = []
    for c in self.liste_chiens:
        if c.id_chien != numero:
            new_liste.append(c)
    self.liste_chiens = new_liste

# avec remove (attention remove)
def retirer_chien(self, numero):
    if chien in self.liste_chiens:
        if chien.id_chien == numero:
            self.liste_chiens.remove(chien)
```

5. (1.0)


```python
eq11.retirer_chien(46)
```

6. (1.0) `chaine[2] + chaine[3]` donne ici `'36'` qui est ensuite convertit en entier avec int().
   Avec le calcul, nous avons `4 + 36 / 60 = 4.6`

7. (2.0)


```python
def temps_course(equipe):
    total = 0 
    for t in equipe.liste_temps : # Parcours de tous les temps de la liste des chaines de caractères
        total = total + convert(t) # incrémentation après conversion
    return total
```


#### Partie C

8. (1.0)

![](img/2024-sujet09-3-8.png)

9. (1.0) Dans un arbre binaire de recherche (ABR) le parcours qui permet d'obtenir les noeuds dans l'ordre croissant est le parcours **infixe**.

10. (1.0) C'est une fonction récursive, car elle s'appelle elle même, dans deux cas différent : un ligne 8, un autre ligne 13 (celle à compléter). De plus nous observons des conditions d'arrêt ligne 5 et ligne 10.

11. (2.0)


```python
def inserer(arb, eq):
    if convert(eq.temps_etape) < convert(arb.racine.temps_etape): # cas où l'insertion se fait dans le sous arbre gauche
        if arb.gauche is None:  # Cas d'arrêt, arbre vide, donc création d'un nouveau noeud
            arb.gauche = Noeud(eq)
        else :
            inserer(arb.gauche, eq)
    else :
        if arb.droit is None:  # Cas d'arrêt, arbre vide, donc création d'un nouveau noeud
            arb.droit = Noeud(eq)
        else :
            inserer(arb.droit, eq)
```

12. (2.0) Pour aller chercher, l'équipe gagnante, nous allons trouver l'équipe la plus à gauche de l'ABR, pour cela nous allons utiliser une fonction recursive qui cherche dans le sous-arbre gauche tant que c'est possible et renvoie la racine quand cela ne l'est plus.


```python
def est_gagnante(arbre):
    if arbre.gauche == None: # Plus de sous-arbre gauche
        return arbre.racine.nom_equipe
    else: #(en raison du return de la ligne précédente, le else est inutile mais fonctionne)
        return est_gagnante(arbre.gauche)
```


#### Partie D

13. (2.0)

![](img/2024-sujet09-3-13.png)

14. (2.0)


```python
def rechercher(arbre, equipe):
    if arbre == None: # Plus rien à chercher, donc équipe non trouvée
        return False
    if arbre.racine == equipe: # équipe trouvée
        return True
    
    # Appels récursifs :
    if convert(equipe.temps_etape) < convert(arbre.racine.temps_etape):
        return rechercher(arbre.gauche, equipe)
    else :
        return rechercher(arbre.droit, equipe)
```
