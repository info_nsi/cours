-- Création de la table joueurs
CREATE TABLE joueurs (
    id_joueur INT PRIMARY KEY,
    nom_joueur VARCHAR(50),
    prenom_joueur VARCHAR(50),
    login VARCHAR(50),
    mdp VARCHAR(50)
);

-- Insertion des données dans la table joueurs
INSERT INTO joueurs (id_joueur, nom_joueur, prenom_joueur, login, mdp) VALUES
(1, 'Dupont', 'Alice', 'alice', '1234'),
(2, 'Durand', 'Belina', 'belina', '5694'),
(3, 'Caron', 'Camilia', 'camilia', '9478'),
(4, 'Dupont', 'Dorine', 'dorine', '1347');

-- Création de la table matchs
CREATE TABLE matchs (
    id_match INT PRIMARY KEY,
    date DATE,
    id_creneau INT,
    id_terrain INT,
    id_joueur1 INT,
    id_joueur2 INT,
    FOREIGN KEY (id_joueur1) REFERENCES joueurs(id_joueur),
    FOREIGN KEY (id_joueur2) REFERENCES joueurs(id_joueur)
);

-- Insertion des données dans la table matchs
INSERT INTO matchs (id_match, date, id_creneau, id_terrain, id_joueur1, id_joueur2) VALUES
(1, '2020-08-01', 2, 1, 1, 4),
(2, '2020-08-01', 3, 1, 2, 3),
(3, '2020-08-02', 6, 2, 1, 3),
(4, '2020-08-02', 7, 2, 2, 4),
(5, '2020-08-08', 3, 3, 1, 2),
(6, '2020-08-08', 5, 2, 3, 4);