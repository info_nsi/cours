-- Création des tables
CREATE TABLE animal (
    id_animal INT PRIMARY KEY,
    nom VARCHAR(50),
    age INT,
    taille FLOAT,
    poids FLOAT,
    nom_espece VARCHAR(50),
    FOREIGN KEY (nom_espece) REFERENCES espece(nom_espece)
);

CREATE TABLE enclos (
    num_enclos INT PRIMARY KEY,
    ecosysteme VARCHAR(50),
    surface FLOAT,
    struct VARCHAR(50),
    date_entretien DATE
);

CREATE TABLE espece (
    nom_espece VARCHAR(50) PRIMARY KEY,
    classe VARCHAR(50),
    alimentation VARCHAR(50),
    num_enclos INT,
    FOREIGN KEY (num_enclos) REFERENCES enclos(num_enclos)
);

-- Remplissage des tables
INSERT INTO animal (id_animal, nom, age, taille, poids, nom_espece) VALUES
(145, 'Romy', 18, 2.3, 130, 'tigre du Bengale'),
(52, 'Boris', 30, 1.10, 48, 'bonobo'),
(225, 'Hervé', 10, 2.4, 130, 'lama'),
(404, 'Moris', 6, 1.70, 100, 'panda'),
(678, 'Léon', 4, 0.30, 1, 'varan');

INSERT INTO enclos (num_enclos, ecosysteme, surface, struct, date_entretien) VALUES
(40, 'banquise', 50, 'bassin', '2024-12-04'),
(18, 'forêt tropicale', 200, 'vitré', '2024-12-05'),
(24, 'savane', 300, 'clôture', '2024-12-04'),
(68, 'désert', 2, 'vivarium', '2024-12-05');

INSERT INTO espece (nom_espece, classe, alimentation, num_enclos) VALUES
('impala', 'mammifères', 'herbivore', 15),
('ara de Buffon', 'oiseaux', 'granivore', 77),
('tigre du Bengale', 'mammifères', 'carnivore', 18),
('caïman', 'reptiles', 'carnivore', 45),
('manchot empereur', 'oiseaux', 'carnivore', 40);
