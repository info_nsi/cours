---
title: un ordonnanceur de processus en Python (POO)
---



L'ordonnancement des processus est préemptif, et un processus ayant un plus petit numéro de priorité est plus prioritaire.
À chaque unité de temps, le processeur choisit d'exécuter le processus actif ayant la plus haute priorité (numéro de priorité le plus petit). Un processus peut être suspendu si un autre plus prioritaire arrive.

### Données des processus :

| Processus | Temps d'exécution | Instant d'arrivée | Numéro de priorité |
|-----------|----------------|----------------|----------------|
| P1        | 3              | 0              | 4              |
| P2        | 4              | 2              | 2              |
| P3        | 3              | 3              | 1              |
| P4        | 4              | 5              | 3              |

### Questions

1. **Réprésenter le chronogramme des 14 temps d'éxécution**

2. **Pour chaque processus, quel est le temps de séjour et le temps d'attente ?**

   - Le temps de séjour pour chaque processus est obtenu soustrayant le temps d’entrée du processus du temps de terminaison.
   
   - Le temps d’attente est calculé soustrayant le temps d’exécution du temps de séjour.


Nous voulons maintenant implémenter cette simulation à l'aide d'un code Python en POO:

3. **Implémentation de la simulation**
    1. Créer une classe `Processus` avec les attributs requis.
    2. Créer une classe `Ordonnanceur` pour gérer les processus et appliquer la stratégie d'ordonnancement.
    3. Implémenter la simulation pas à pas, avec une boucle qui ajoute à chaque instant un processus rentrant dans la file de l'ordonnanceur et execute tour à tour un processus durant un quantum de temps. 





