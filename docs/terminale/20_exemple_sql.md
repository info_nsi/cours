---
title: Un exemple en SQL
---

Avec les livres de l'introduction

## Ecrire ici le  code SQL, puis l'exécuter, en ayant chargé le code des relations nécessaires

{!{ sqlide titre="Votre code SQL" espace="exercices_sql"}!}

## La relation livres avec les auteurs: 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="terminale/sql/livres_nonNF.sql" espace="exercices_sql"}!}

## La relation auteurs: 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="terminale/sql/auteurs.sql" espace="exercices_sql"}!}

## La relation livres avec une clé étrangère: 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="terminale/sql/livres_NF.sql" espace="exercices_sql"}!}
