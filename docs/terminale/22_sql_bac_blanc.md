---
title: SQL Tennis du bac blanc
---


### Ecrire ici le  code SQL, puis l'exécuter, en ayant chargé le code des relations nécessaires

{!{ sqlide titre="Votre code SQL" espace="exercices_sql"}!}

### Les relation du tennis: 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="terminale/sql/tennis.sql" espace="exercices_sql"}!}
