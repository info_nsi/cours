---
title: Jeu de la vie
---

```python
import random

class Cellule:
    
    def __init__(self):
        self.etat = random.randint(0,1)
        self.voisin = []
        
    def evolue(self):
        '''
        modifie l'état de la cellule en fonction du nombre de voisins
        Si elle a exactement 3 voisins elle vit
        Si elle a exactement deux voisins elle reste en vie si elle l'était
        Sinon elle meurt
        '''
        # A écrire
        
    

def debut_jeu():
    '''
    Retourne une liste de liste de 3 x 3 Cellules
    '''
    return [[Cellule() for _ in range(3)]for _ in range(3)]
    
# initialise le jeu
jeu = debut_jeu()
    
def ajout_voisins(jeu):
    '''
    jeu est une liste de listes de Cellules
    On va la parcourir pour ajouter des tuples aux voisins concernés
    '''
    # Parcours des cellules
    for li in range(3):
        for co in range(3):
            # décalage voisinage
            for delta_l in [-1,0,1]:
                for delta_c in [-1,0,1]:
                    if delta_c != 0 or delta_l != 0:
                        # est-ce que la cellule est vivante
                        if jeu[li][co].etat:
                            # est-ce que "voisin" est dans la grille
                            if (li + delta_l) in range(3) and (co + delta_c) in range(3):
                                jeu[li + delta_l][co + delta_c].voisin.append((li,co))
                        
        

def affichage(jeu):
    '''
    Affiche la grille
    un espace pour les cellules mortes
    un * pour les cellules vivantes
    '''
    # A écrire

def evolue(jeu):
    '''
    applique l'évolution à toutes les cellules en modifant les états
    remets l'attribut voisin à chaque génération
    Il faut donc deux boucles de parcours
    '''
    # A écrire
```