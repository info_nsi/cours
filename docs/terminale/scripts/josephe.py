def next_kill(soldiers:list, k:int, last_kill_ind = -1):
    '''
    ...A compléter
    '''
    assert k >= 1 # ...
    return (last_kill_ind + k) % ...


def josephe(n,k):
    '''
     ...A compléter
    '''
    assert n > 0  # ....

    soldiers =  # mettre ici la réponse de la question 1) qui génère la liste [1;2,...,n]
    
    ind = -1 
    while len(soldiers) > 1:
        ind = ...
        soldiers.pop(...) # élimination du soldat d'indice ind
        ind -= 1 # La décrémentation de ind sert a prendre en compte l'élimination de soldiers[ind]
    
    return soldiers[0]


# Test du code écrit
assert josephe(41,3) == 31
assert josephe(10,3) == 4