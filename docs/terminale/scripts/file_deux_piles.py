

# --------- PYODIDE:env --------- #

def creer_pile_vide():
    '''Renvoie une liste vide'''
    return ()

def est_pile_vide(p):
    '''Renvoie True si la pile est vide'''
    return p == ()

def empiler(elt, p) :
    '''
    Renvoie une nouvelle pile oÃ¹ elt est le sommet
    et pile la suite de notre nouvelle pile.
    '''
    return (elt, p)

def lire_sommet(p):
    '''Renvoie la valeur stockÃ©e au sommet d'une pile NON VIDE'''
    return p[0]

def depiler(p):
    '''
    Renvoie une nouvelle pile oÃ¹ on a supprimÃ© l'ancien sommet
    d'une pile NON VIDE
    '''
    return p[1]

# --------- PYODIDE:code --------- #

def creer_file_vide():
    '''Renvoie une file vide'''
    ...

def est_vide(file):
    '''Renvoie True si la file est vide'''
    ...

def enfiler(file, valeur):
    '''Ajoute l'élément valeur à la file'''
    ...

def defiler(file):
    '''
    Défile un élément de la file
    Renvoie cet élément
    '''
    ...

# --------- PYODIDE:corr --------- #

def creer_file_vide():
    '''Renvoie une file vide'''
    return [creer_pile_vide(), creer_pile_vide()]

def est_file_vide(file):
    '''Renvoie True si la file est vide'''
    return est_pile_vide(file[0]) and est_pile_vide(file[1])

def enfiler(file, valeur):
    '''Ajoute l'élément valeur à la file'''
    file[0] = p.empiler(valeur, file[0])

def defiler(file):
    '''
    Défile un élément de la file
    Renvoie cet élément
    '''
    if p.est_pile_vide(file[1]):
        while not p.est_pile_vide(file[0]):
            valeur = p.lire_sommet(file[0])
            file[0] = p.depiler(file[0])
            file[1] = p.empiler(valeur, file[1])
    avant = p.lire_sommet(file[1])
    file[1] = p.depiler(file[1])
    return avant

# --------- PYODIDE:tests --------- #

ma_file = creer_file_vide()
enfiler(ma_file, 1)
enfiler(ma_file, 2)
enfiler(ma_file, 3)
enfiler(ma_file, 4)
enfiler(ma_file, 5)
assert defiler(ma_file) == 1
assert defiler(ma_file) == 2

# --------- PYODIDE:secrets --------- #


# autres tests