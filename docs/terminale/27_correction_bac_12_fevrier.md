---
title: Correction Bac Blanc 12 février 2025
---

Voici le [Sujet](./pdf/Bac_blanc_12_fevrier.pdf)

À partir de deux exercices de sujets de 2023.


## Exercice 1 (original : 3 de 23-NSIJ2PO1)

1. a

Voici l'arbre des PID liès à firefox

![](img/firefox.png)


b. La commande qui a lancé *firefox* est `/usr/lib/firefox/firefox` qui a été lancée à partir du `bash` (PID du père)


c. Pour supprimer toutes les instance de `firefox` il faut utiliser `kill`sur le PID du processus père. La commande des donc

`kill 9617`



2.

a.

![](img/état_processus.png)

**Remarque** La flèche `Élu` -> `Prêt` n'était pas demandée mais elle est importante pour les transitions possibles des processus.

b.

| Processus | Temps d’exécution |
|-----------|------------------|
| P1        | 10 - 0 = 10      |
| P2        | 18 - 2 = 16      |
| P3        | 5 - 3 = 2        |
| P4        | 9 - 7 = 2       |

Moyenne des temps d’exécutions = $\frac{10+16+2+2}{4} = \frac{32}{8} = 8$


c. Voici le chronogramme si le *quantum* est de deux cylces.

| P1  | P1  | P1  | P1  | P3  | P3  | P1  | P1  | P1  | P1  | P4  | P4  | P2  | P2  | P2  | P2  | P2  | P2  |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|



d.

| Processus | Temps d’exécution |
|-----------|------------------|
| P1        | 10 - 0 = 10      |
| P2        | 18 - 2 = 16      |
| P3        | 6 - 3 = 3        |
| P4        | 12 - 7 = 5       |

Moyenne des temps d’exécutions = $\frac{10+16+3+5}{4} = 8.5$


La moyenne est supérieure, cet ordonnancement est donc moins performant.

3.

a.

```python
def choix_processus(liste_attente):
'''
Dans ce code nous supposons que les processus non encore arrivés ne sont pas
dans liste_attente

Les codes qui vérifient tous les processus avec un test supplémentaire sont aussi valables
'''
    if liste_attente != []:
        mini = len(liste_attente[0])
        indice = 0
        for i in range(1, len(liste_attente)):
            temps_restant = len(liste_attente[i])
            if temps_restant < mini:
                mini = temps_restant
                indice = i
        return indice
```

b.

```python
def ordonnancement(liste_proc):
    execution = [] # Liste pour contenir les cycles d'éxécution
    attente = scrutation(liste_proc, []) # choix des processus présents
    
    # L'ordonnanceur tourne tant qu'il y a des processus en attente
    while attente != []:
        indice = choix_processus(attente)
        
        if attente[indice] == []: # Processus terminé
            attente.pop(indice)
        
        else:
            cycle = attente[indice].pop() # retire le dernier élément non vide du processus
            execution.append(cycle) # ajout de l'élément à execution
            
        attente = scrutation(liste_proc, attente) # vérification de l'arrivée possible de processus

    return execution
```


## Exercice 2 (original 3 de 23-NSIJ2G11)

1.

a.

`bonjour('Alan')` retourne `'Bonjour Alan !'` 

b.

Les expressions Python avec `==` retournent des booléens (<class 'bool'>)

`x` a pour valeur `False` car `'n' != 'j'`

`y` a pour valeur `True` car `'o' == 'o'`

c.

```python
def occurrences_lettre(une_chaine, une_lettre):
    nb = 0
    for caractere in une_chaine:
        if caractere == une_lettre:
            nb += 1
    return nb
```

2.

a.

Voici un exemple, d'autres arbres de même hauteur sont possibles du moment qu'il respecte l'ordre d'un ABR, ici l'ordre alphabétique.

![](img/arb_min.png)

b.

Deux arbres filiformes sont possibles, un qui part vers la droite et qui commence par le premier mot en ordre alphabétique `chameau`, et un autre qui part vers la gauche et qui commence donc par `renard`.

Voici celui qui commence par chameau.

![](img/arb_max.png)

3.

a. La fonction `mystere()` compte le nombre total de noeuds de l'arbre, c'est donc la **taille** de l'arbre.

` mystere(abr_mots_francais)` retourne donc `336531`

b.

Voici le code en définissant la hauteur d'un arbre vide à -1 et un celle d'un arbre racine à 0.

```python
def hauteur(un_abr):
    if un_abr.est_vide():
        return -1  # Convention : hauteur d'un arbre vide = -1
    else:
        return 1 + max(hauteur(un_abr.sous_arbre_gauche), hauteur(un_abr.sous_arbre_droit))
```

4.

a.

```python
def chercher_mots(liste_mots, longueur, lettre, position):
    res = []
    for i in range(len(liste_mots)):
        if len(liste_mots[i]) == longueur and len(liste_mots[i]) > position and liste_mots[i][position] == lettre:
            res.append(liste_mots[i])
    return res
```

b.

Nous avons ici un appel de fonction imbriqué, c'est à dire que le résultats de la fonction est passée en entrée de la seconde fonction.

Cela fonctionne aussi avec les deux fonctions identiques.

**Remarque** Cela ne signifie pas que le code de la fonction `chercher_mots()` soit récursif.

C'est uniquement le cas lorsque que la fonction est appelée dans son propre code.


`chercher_mots(chercher_mots(liste_mots_francais,3,'x',2),3,'a',1)` retourne les mots de 3 lettres, terminant par 'x' et ayant un 'a' en deuxième lettre.

C'est à dire les mots de 3 lettres se terminant par 'ax'

c.

Nous pouvons appliquer 3 fois la fonction `chercher_mots()`

`chercher_mots(chercher_mots(chercher_mots(liste_mots_francais, 5, 't', 2), 5, 'e', 3), 5, 'r', 4)`

**Remarque** Il faut veiller à ce que le deuxième paramètre de `chercher_mots` soit toujours 5, sinon nous aurions une liste vide.
