---
title: Base de données d'un Zoo
---

# Contenu des relations de la base de données du zoo

## Table `animal`
| id_animal | nom   | age | taille | poids | nom_espece          |
|-----------|-------|-----|--------|-------|---------------------|
| 145       | Romy  | 18  | 2.3    | 130   | tigre du Bengale    |
| 52        | Boris | 30  | 1.10   | 48    | bonobo              |
| ...       | ...   | ... | ...    | ...   | ...                 |
| 225       | Hervé | 10  | 2.4    | 130   | lama                |
| 404       | Moris | 6   | 1.70   | 100   | panda               |
| 678       | Léon  | 4   | 0.30   | 1     | varan               |

## Table `enclos`
| num_enclos | ecosysteme       | surface | struct   | date_entretien |
|------------|------------------|---------|----------|----------------|
| 40         | banquise         | 50      | bassin   | 04/12/2024     |
| 18         | forêt tropicale  | 200     | vitré    | 05/12/2024     |
| ...        | ...              | ...     | ...      | ...            |
| 24         | savane           | 300     | clôture  | 04/12/2024     |
| 68         | désert           | 2       | vivarium | 05/12/2024     |

## Table `espece`
| nom_espece          | classe      | alimentation | num_enclos |
|---------------------|-------------|--------------|------------|
| impala              | mammifères  | herbivore    | 15         |
| ara de Buffon       | oiseaux     | granivore    | 77         |
| ...                 | ...         | ...          | ...        |
| tigre du Bengale    | mammifères  | carnivore    | 18         |
| caïman              | reptiles    | carnivore    | 45         |
| manchot empereur    | oiseaux     | carnivore    | 40         |


## Schéma relationnel
1. **Écrire le schéma relationnel de cette base en indiquant les clés primaires en les soulignant et en indiquant les clés étrangère en les précédant d'un #.**

2. **Qu'impose les contraintes d'intégrité sur la modification ou suppression des clés primaires ?** 

## Questions SQL sur la consultation de la base

### Questions sans jointure

1. **Lister les noms et les âges de tous les animaux.**

??? important "Réponse"

    ```sql
    SELECT nom, age FROM animal;
    ```

2. **Trouver les enclos ayant une surface supérieure à 100 m².**

??? important "Réponse"

    ```sql
    SELECT * FROM enclos WHERE surface > 100;
    ```

3. **Afficher tous les noms d'espèces appartenant à la classe "mammifères".**

??? important "Réponse"

    ```sql
    SELECT nom_espece FROM espece WHERE classe = 'mammifères';
    ```

4. **Afficher les informations sur les enclos entretenus le 04/12/2024.**

??? important "Réponse"

    ```sql
    SELECT * FROM enclos WHERE date_entretien = '2024-12-04';
    ```

5. **Trouver les noms des animaux pesant plus de 50 kg.**

??? important "Réponse"

    ```sql
    SELECT nom FROM animal WHERE poids > 50;
    ```


### Questions avec jointure

1. **Afficher le nom des animaux et l'écosystème de leur enclos.**

??? important "Réponse"

    ```sql
    SELECT animal.nom, enclos.ecosysteme
    FROM animal
    JOIN espece ON animal.nom_espece = espece.nom_espece
    JOIN enclos ON espece.num_enclos = enclos.num_enclos;
    ```

2. **Lister les espèces carnivores et les types de structure de leurs enclos.**

??? important "Réponse"

    ```sql
    SELECT espece.nom_espece, enclos.struct
    FROM espece
    JOIN enclos ON espece.num_enclos = enclos.num_enclos
    WHERE espece.alimentation = 'carnivore';
    ```

3. **Trouver les noms et poids des animaux vivant dans la "forêt tropicale".**

??? important "Réponse"

    ```sql
    SELECT animal.nom, animal.poids
    FROM animal
    JOIN espece ON animal.nom_espece = espece.nom_espece
    JOIN enclos ON espece.num_enclos = enclos.num_enclos
    WHERE enclos.ecosysteme = 'forêt tropicale';
    ```

4. **Afficher les noms d'animaux et leurs classes taxonomiques.**

??? important "Réponse"

    ```sql
    SELECT animal.nom, espece.classe
    FROM animal
    JOIN espece ON animal.nom_espece = espece.nom_espece;
    ```

5. **Lister les enclos avec leur surface et les espèces qu'ils abritent.**

??? important "Réponse"

    ```sql
    SELECT enclos.num_enclos, enclos.surface, espece.nom_espece
    FROM enclos
    JOIN espece ON enclos.num_enclos = espece.num_enclos;
    ```


## Ecrire ici le  code SQL, puis l'exécuter, en ayant chargé le code des relations nécessaires

{!{ sqlide titre="Votre code SQL" espace="exercices_sql"}!}

## La relation livres avec les auteurs: 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="terminale/sql/zoo.sql" espace="exercices_sql"}!}

