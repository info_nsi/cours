def pgcd(a,b):
    if b == 0:
        return a # cas de base, condition d'arrêt
    if a < b:
        return pgcd(b,a) # Pour garder a >= b, en permutant a et b
    return ... # a compléter