# --------- PYODIDE:code --------- #

def is_prime(n):
    if ...:
        return False # n est divisible par 2 ou est 0 ou 1
    div = ... # pour tester les diviseurs supérieurs ou égaux à 3
    while .... <= n:
        if ....: # n est divisible par div
            return ....
        div += ...
    return True



# --------- PYODIDE:corr --------- #

def is_prime(n):
    if (n % 2 == 0 and n != 2) or n == 1:
        return False # n est divisible par 2, est 0 ou 1
    div = 3 # pour tester les diviseurs supérieurs ou égaux à 3
    while div ** 2 <= n:
        if n % div == 0: # n est divisible par div
            return False
        div += 2
    return True



# --------- PYODIDE:secrets --------- #


# autres tests


assert is_prime(5) is True
assert is_prime(2) is True
assert is_prime(11) is True
assert is_prime(4) is False
assert is_prime(1) is False
assert is_prime(0) is False