---
title: Modules Numpy et Matplotlib
---

# Les tableaux numpy.array
Le module `numpy` permet de manipuler les tableaux de données, données qui ont toutes le même type numérique.

Le nombre de dimension est variable. Ce module est particulièrement optimisé et est au centre de tous les modules de calculs scientifiques en `python`. 

Il faut donc connaitre ce module et savoir utiliser la structure `numpy.array`.

Nous avons besoin d'importer le module avec l'instruction :

```python
	import numpy as np
```

### Création et propriétés

Création à partir d’une liste de listes. Sans option supplémentaire le type est détecté automatiquement.
```python
a = np.array([[8,3,2,4], [5,1,6,0], [9,7,4,1]])
print(a)

[[8 3 2 4]
 [5 1 6 0]
 [9 7 4 1]]
```

**Remarque**: le type entier utilisé dans numpy est le type numpy.int64 c’est à dire un entier codé sur 64 bits et le nombre le type int par défaut de python.

!!! remarque

	les tableaux `numpy` fonctionnent comme des vecteurs
	```python
		v = np.array([1,2,3])
		y = 2 * v
		print(y)
		```

		affiche
		[2 4 6]
		

## Création à partir de fonctions numpy
```python
np.array([ i+1 for i in range(10) ])

array([ 1,  2,  3,  4,  5,  6,  7,  8,  9, 10])
```


```python
np.arange(10, 20, step=2)    # même syntaxe que range 

array([10, 12, 14, 16, 18])
```

# Module `matplotlib`

C’est le module de tracé qu’il faut connaitre. D’autres modules comme seaborn sont plus adaptés aux statistiques mais reposent sur matplotlib.
```python
import numpy as np
import matplotlib.pyplot as plt
```


Ce module permet de tracer des corubes et des nuages des points

## Courbes
Nous allons définir les couples `(x,y)`

```python
x = np.arange(0,6,0.1)
y = np.sin(x)
```

Et l'instruction `plt.plot(x,y)` permet de tracer la courbe

![](img/sin.png)

## Nuages de points
```python
x = np.arange(1,11)
y = 2 ** x
```

Et toujours avec `plt.plot(x,y)`

![](img/puissance2.png)
