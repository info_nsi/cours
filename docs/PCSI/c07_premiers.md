---
title: Conjectures sur les nombres premiers
---

??? note "sujet DS"

    <div class="centre">
	<iframe 
	src="../ITC-MPSI-DS1.pdf".pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


# Tester si un nombre est premiers
Nous avons besoin d'une fonction `is_prime` qui retourne `True` si  le nombre est premier.

`False` sinon.

{{ IDE("scripts/is_prime")}}




```python

import numpy as np # Non utilisée ici mais classique
import matplotlib.pyplot as plt # Syntaxe habituelle pour obtenir des graphiques

def is_prime(n):
    ...



# Corrections autres fonctions 
def Goldbach(n: int):
    if (n  % 2) or (n < 4):
        return False
    for i in range(2,(n // 2) + 1): # pas besoin de dépasser n // 2
        if is_prime(i) and is_prime(n-i):
            return (i,n-i)
    return False # dans ce cas, nous avons un contre exemple à la conjecture de Goldbach

def hauteur(n):
    return Goldbach(n)[0]
    

def Gold_max(N):
    L = [n for n in range(4,N+1,2)] # liste des nombres pairs entre 4 à N
    D = {} # Initialisation du dictionnaire
    for k in L:
        D[k] = Goldbach(k)
    
    return D # D contient les éléments de la forme nombre pair n : hauteur de n


def affiche(N):
    x = [k for k in Gold_max(N).keys()]
    y = [v[0] for v in Gold_max(N).values()] # v[0] est la hauteur dans le coupe (n,m) de Goldbach
    plt.scatter(x,y)
	
```