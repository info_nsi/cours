---
title: Traitement des images
---

Nous allons effectuer différents traitements sur l'image suivante:

![](img/cours_info.jpg)

### Les modules nécessaires :

**3 modules :**

- matplotlib.image que nous utilisons pour charger les images depuis un fichier

- matplotlib.pyplot, c'est le module classique pour afficher des courbes, des graphiques

- numpy  permet de manipuler les tableaux multidimensionnels, ce qui est nécessaire pour effectuer des transformations sur les pixels d'une image.

Une importation standard s'écrit :

```python
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
```



Nous souhaitons écrire le code Python pour transformer l'image.

!!! important "3 transformations : niveau de gris, négatif, hompthétie"
    #### 1. Conversion en niveaux de gris :

    L'image en niveaux de gris est obtenue en supprimant les informations de couleur, ne laissant que l'intensité lumineuse. 

    Cela peut être accompli en calculant une moyenne simple ou pondérée des trois canaux RGB (Rouge, Vert, Bleu) de chaque pixel, ou en utilisant une formule standard telle que :

    Niveau de gris=0.2989×R+0.5870×G+0.1140×B


    Cette opération permet de représenter l'image sur une seule dimension d'intensité lumineuse.

    #### 2. Transformation négative :

    La transformation négative consiste à inverser les couleurs d'une image. Pour chaque pixel, les nouvelles valeurs des canaux de couleur sont calculées comme suit :

    Comporsante R,V,B du nouveau pixel = 255−Valeur originale du pixel # pour une valeur entière de chaque composante R, V, B


    Cette opération renverse les couleurs de l'image, par exemple, le blanc devient noir, et le bleu devient orange, etc.


    ####  3. Transformation d'homothétie :

    Une homothétie est une transformation géométrique qui modifie la taille de l'image de manière proportionnelle tout en conservant ses proportions.

    Si on applique une homothétie avec un facteur k, chaque pixel de l'image peut être redimensionné pour être plus petit ou plus grand. En pratique, cela se traduit souvent par un redimensionnement de l'image en ajustant sa résolution.

    Nous allons ici étudier uniquement les réductions avec un facteur k et une nouvelle taille 1 / k.


### Code à compléter

Le code suivant est à compléter et tester :

```python
    # Imprtation des modules
    import matplotlib.image as mpimg
    import matplotlib.pyplot as plt
    import numpy as np

    # chargement de l'imaage, attention à la mettre dans le même dossier
    img = mpimg.imread("./cours_info.jpg")



    # img.ndim  vaut 3 

    # img.shape est un tuple de 3 tailles : hauteur en pixels, largeur en pixel, et 3 pour chaque composante.
    # avec l'image de l'exemple img.shape = (582, 932, 3)


    # image en niveau de gris
    def to_bw(img):
        img_transformee = np.ndarray(img.shape) # Création d'une nouvelle image aux mêmes dimensions

        # Pacours des lignes et colonnes
        for line in range(img.shape[0]):
            for col in range(img.shape[1]):

                gris = ... # calculs de la moyenne simple ou pondérée

                img_transformee[line][col]=  np.array([gris,gris,gris])
        return img_transformee


    # image en négatif
    def to_negative(img):
        img_transformee = np.ndarray(img.shape)
        for line in range(img.shape[0]):
            for col in range(img.shape[1]):

                # Calculs des composantes R,V,B de chaque pixel
                #r = 
                #v = 
                #b =
                # a compléter 

                img_transformee[line][col]=  np.array([r,v,b])
        return img_transformee

    # le code suivant est complet
    # Un premier exemple : réduction de la taille de moitié
    def to_half_size(img):
        nline = img.shape[0] // 2
        ncol =  img.shape[1] // 2
        img_transformee = np.ndarray((nline, ncol, 3))
        for line in range(nline):
            for col in range(ncol):
                r = (img[line * 2][col * 2][0]) / 256
                v = (img[line * 2][col * 2][1]) / 256
                b = (img[line * 2][col * 2][2]) / 256
                img_transformee[line][col]=  np.array([r,v,b])
        return img_transformee


    # Réduction de la taille par k
    def to_reduce_factor(img, factor:int):
        nline = img.shape[0] // factor
        ncol =  img.shape[1] // factor
        img_transformee = np.ndarray((nline, ncol, 3))
        for line in range(nline):
            for col in range(ncol):
                    
                    #
                    # à compléter
                    #
                img_transformee[line][col]=  np.array([r,v,b])
        return img_transformee

    def to_reduce_factor_smooth(img, factor:int):
        nline = img.shape[0] // factor
        ncol =  img.shape[1] // factor
        img_transformee = np.ndarray((nline, ncol, 3))
        for line in range(nline):
            for col in range(ncol):

                    # a compléter avec un calcul de lissage sur plusieurs pixels

                img_transformee[line][col]=  np.array([r,v,b])
        return img_transformee
        

    # Affichage des résultats

    img_transformee = to_half_size(img) # exemple avec to_half_size(img)
    plt.imshow(img_transformee)
    plt.show()
```


### Les résultats

#### En niveau de gris

![](img/Figure_gris_moy_simple.png)

#### En négatif

![](img/Figure_negatif.png)

#### Réduction par 15 naïve

![](img/Figure_reduce_15.png)


#### Réduction par 15 avec lissage

![](img/Figure_reduce_15_smooth.png)
