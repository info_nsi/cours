-- Création de la table Photo
CREATE TABLE Photo (
    PH_id INT PRIMARY KEY,
    PH_date DATETIME,
    PH_larg INT,
    PH_haut INT,
    PH_auteur INT,
    PH_fichier VARCHAR(255),
    FOREIGN KEY (PH_auteur) REFERENCES Personne(PE_id)
);

-- Création de la table Personne
CREATE TABLE Personne (
    PE_id INT PRIMARY KEY,
    PE_sexe CHAR(1) CHECK (PE_sexe IN ('M', 'F')),
    PE_prenom VARCHAR(100)
);

-- Création de la table Motcle
CREATE TABLE Motcle (
    MC_id INT PRIMARY KEY,
    MC_texte VARCHAR(255)
);

-- Création de la table Decrit
CREATE TABLE Decrit (
    MC_id INT,
    PH_id INT,
    PRIMARY KEY (MC_id, PH_id),
    FOREIGN KEY (MC_id) REFERENCES Motcle(MC_id),
    FOREIGN KEY (PH_id) REFERENCES Photo(PH_id)
);

-- Création de la table Present
CREATE TABLE Present (
    PE_id INT,
    PH_id INT,
    PRIMARY KEY (PE_id, PH_id),
    FOREIGN KEY (PE_id) REFERENCES Personne(PE_id),
    FOREIGN KEY (PH_id) REFERENCES Photo(PH_id)
);

