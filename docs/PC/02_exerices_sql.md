---
title: Centrale 2020 - Partie SQL
---

[Sujet intégral Informatique Centrale 2020](https://www.concours-centrale-supelec.fr/CentraleSupelec/2020/Multi/I001.pdf)


[Rapport sur ce sujet](https://www.concours-centrale-supelec.fr/CentraleSupelec/2020/PC/PCe-Info.pdf)

## Partie SQL
## Base de données pour choisir des photos

Une première étape dans la conception d’une photomosaïque est le choix d’une image source et de vignettes.
Cette partie est consacrée à la sélection d’images dans la banque.
Les images de la banque sont répertoriées dans une base de données dont le modèle physique est présenté figure 4, dans laquelle les clés primaires sont notées en italique.

![](img/base-photo-centrale-2020.png)

## Ecrire votre propre code, puis l'exécuter

{!{ sqlide titre="Votre code SQL" espace="exercices_sql"}!}

## Les relations de l'exercice: 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="PC/sql/centrale2020-tables.sql" espace="exercices_sql"}!}

## Les contenu des relations: 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="PC/sql/centrale2020-contenus.sql" espace="exercices_sql"}!}

### Questions

=== "Les questions"

Q 19.
Écrire une requête SQL donnant les identifiants de toutes les photographies au ratio 4:3, c’est-à-dire
dont le rapport largeur sur hauteur vaut exactement 4/3.


Q 20.
Écrire une requête qui compte le nombre de photos prises par « Alice » ou « Bernard ».


Q 21.
Écrire une requête qui fournit l’identifiant et la date des photographies prises avant 2006 et associées au mot-clé « surf » 


Q 22.
Écrire une requête qui donne le prénom de l’auteur et l’identifiant de tous les selfies, c’est-à-dire les photographies sur lesquelles l’auteur est présent.



Q 23.
Écrire une requête qui sélectionne toutes les photographies sur lesquelles sont présents « Alice » et
« Bernard », à l’exclusion de toute autre personne.


=== "Réponses"

Q 19.

SELECT PH_id FROM Photo WHERE PH_larg * 4 = PH_haut * 3 OR PH_larg * 3 = PH_haut * 4

Q 20.

SELECT COUNT(*) FROM Photo JOIN Personne ON PH_auteur = PE_id WHERE PE_prenom = "Alice" OR PE_prenom = "Bernard"


Q 21.

SELECT PH_id,PH_date From Photo NATURAL JOIN Decrit NATURAL JOIN Motcle WHERE MC_texte = "surf" AND PH_date < '2006-01-01'


Q 22.
SELECT PH_id, PE_prenom FROM Photo JOIN Personne ON PH_auteur = PE_id NATURAL JOIN Present WHERE PH_auteur = Present.PE_id


Q 23.

-- Relation des photos ayant exactement deux personnes présentes

SELECT PH_id FROM Photo NATURAL JOIN Present NATURAL JOIN Personne GROUP BY PH_id HAVING COUNT(PE_id) = 2

EXCEPT

-- Relation des photos qui contiennent des personnes autre que Bernard ou Alice

SELECT PH_id FROM Photo NATURAL JOIN Present NATURAL JOIN Personne WHERE NOT PE_prenom IN ("Alice", "Bernard")
