---
title: 🏡 Accueil
---


!!! info "👩‍💻 Pratiquer et progresser en informatique 👨‍💻"

   Vous trouverez ici les ressources pour les cours de NSI et ITC.

   C'est aussi le lieu pour s'entrâiner.
   Vous pouvez compléter les exercices proposés au sein des pages de votre niveau.
   Le bac à sable permet de tester vos codes Python.

   La maîtrise de l'informatique c'est avant tout une question de pratique !


 **✨ Bon code  ! ✨**
